# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is used for Assignment A3.2.
The application is built on a Asynchronous Communication
using Messaging architecture, that enables the client to send messages asynchronously through a Middleware by RabbitMq.   

The application is used to message clients of an online dvd shop evry time a new dvd is added to the store. 
The clients receive emails with every dvd added, and a new text file is created containing the information about the dvd.

### How do I get set up? ###
  
This application does require a local tomcat server and a server by RabbtMq. 
  
To use it, follow the next steps:
	1. download the source code  
	2. import the project into an IDE (Eclipse or  IntelliJ)  
	3. perfom Maven install on the main maven module (root)
	4. install a local tomcat server (e.g tomcat 3.5.4) and create a tomcat configuration in IntelliJ or eclipse 
		(the exploded war created by the business module must be run on the tomcat server). 
		Also, the start path for the tomcat server must be 'http://localhost:3000/dvd'.
	5. Run the main function in ConsumerStart. (start the client queue, so that it is listening for messages).
	6. Run the tomcat server.
  	
The two functions are run as following:  
1. Add a new DVD
	- once the program runs sunccesfully, on the path: 'http:/localhost:8080/dvd' a form will appear.
	- write the needed data in the form and press sumbmit.
	- an email is sent to all theclients and a file is created ont the path: 'ds2018_30441_anda_moldovan_assignment_3/_files'
  
### Who do I talk to? ###

Repo owner: Anda Moldovan