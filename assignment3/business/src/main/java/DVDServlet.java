import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

public class DVDServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String form = "<form action=\"/dvd\" method=\"post\" > " +
                        " Title:  " +
                        " <input type=\"text\" name=\"title\"> <br> " +
                        " Year:  " +
                        " <input type=\"text\" name=\"year\"> <br>" +
                        " Price:  " +
                        " <input type=\"text\" name=\"price\"> <br> " +
                        " <input type=\"submit\" value=\"Add DVD\">" +
                        " </form>";
        out.println(form);

        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        String title = request.getParameter("title");
        String year = request.getParameter("year");
        String price = request.getParameter("price");


        if(!title.equals("") && year != null && price != null){
            DVD dvd = new DVD(title, Integer.parseInt(year), Double.parseDouble(price));

            ProducerStart producer = new ProducerStart();
            try {
                producer.startProducer(dvd);
            } catch (TimeoutException e) {
                e.printStackTrace();
            }


            String alert = "<script alert(\"DVD Saved\") />";
            PrintWriter out = response.getWriter();
            out.println(alert);

        }
        response.sendRedirect("/dvd");
      //  doGet(request, response);
    }
}
