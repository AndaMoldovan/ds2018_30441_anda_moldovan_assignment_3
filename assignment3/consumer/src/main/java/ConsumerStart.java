import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ConsumerStart {

    public final static String QUEUE_NAME = "DVD_QUEUE";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [x] Waiting for messages");

        final FileMaker fileMaker = new FileMaker("D:/Users/andam/Documents/Anul IV/DS/Assignment 3_2/ds2018_30441_anda_moldovan_assignment_3/_files");
        final MailSender mailSender = new MailSender();
//        Consumer consumer = new DefaultConsumer(channel){
//            @Override
//            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
//                String message = new String(body, "utf-8");
//                System.out.println(" [x] Received '" + message + "'");
//            }
//        };
//
//        channel.basicConsume(QUEUE_NAME, true, consumer);

        Consumer fileConsumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
               String dvdName = new String(body, "utf-8");
               System.out.println(" [x] Received '" + dvdName + "'");
               fileMaker.createTextFile(dvdName.split("\n")[0].substring(7), dvdName);
               mailSender.sendMail("andamold@gmail.com", "New Movie Added", dvdName);
            }
        };
        channel.basicConsume(QUEUE_NAME, true, fileConsumer);

    }

    public void startConsumer(){

    }
}
