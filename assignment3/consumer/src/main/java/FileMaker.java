import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileMaker {

    private String filePath;

    public FileMaker(String filePath){
        this.filePath = filePath;
    }

    public void createTextFile(String fileName, String message) throws IOException {
        Files.write(Paths.get(filePath + "/" + fileName  + ".txt"), message.getBytes());
    }
}
